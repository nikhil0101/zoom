<!DOCTYPE html>
<html>
    <head>
        <title>Zoom - @yield('title')</title>

        @include('includes.head')
    </head>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
            <!-- Navbar -->
            @include('includes.header')
            <!-- /.navbar -->
            <!-- Main Sidebar Container -->
            <!-- Sidebar -->
            @include('includes.sidebar')
            <!-- /.sidebar -->
            <!-- Content Wrapper. Contains page content -->
            
            <div class="content-wrapper">
                @yield('content')
            </div>

            @include('includes.footer')
    </body>
</html>