@extends('layouts.master')
@section('title', 'Edit User')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">    
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <!-- <h1></h1> -->
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Profile</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="col-12">
            @if ($errors->has('update'))
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ $errors->first('update') }}                            </div>
            @endif
            @if ($errors->has('duplicate_email'))
            <div class="alert alert-danger">
                {{ $errors->first('duplicate_email') }}
            </div>
            @endif
            <div class="card card-info">             
                <div class="card-header">
                    <span class="float-sm-left">Profile</span>
                    <span class="float-sm-right">
                        <a href="javascript:void(0)" onclick="" id="editProfile"></a>

                    </span>
                </div>
                <div class="card-body register-card-body">
    <!-- <p class="login-box-msg">Please add event details</p> -->
                    {{ Form::open(array('url' => 'updateuser','id' => 'myProfileform', 'name' => '', 'method' => 'post')) }}
                    @csrf
                    <div class="form-group has-feedback">
                        <input type="hidden" class="form-control" name="id" value="{{ $user->id }}"placeholder="Id">
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-1 col-form-label">Name</label>
                        <div class="col-sm-11">
                            <input type="text" class="form-control" name="fullname" value="{{ $user->full_name }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-1 col-form-label">Email</label>
                        <div class="col-sm-11">
                            <input type="email" class="form-control" name="email" value="{{ $user->email }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-1 col-form-label">Phone</label>
                        <div class="col-sm-11">
                            <input type="text" class="form-control" name="phone" value="{{ $user->phone }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-1 col-form-label">Address</label>
                        <div class="col-sm-11">
                            <textarea class="form-control" rows="3" name="address"> <?php echo $user->address; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-1 col-form-label">User Role</label>
                        <div class="col-sm-11">
                            <p class="text-success" style="margin-top: 7px; font-weight: bold;">{{ $user->Role->role_name }}</p>
                            <input type="hidden" class="form-control" name="user_roll" value="{{ $user->user_roll }}"placeholder="Id">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-1 col-form-label">Status</label>
                        <div class="col-sm-11">
                            @if($user->status == 1)
                            <p class="text-success" style="margin-top: 7px; font-weight: bold;">Active</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <input type="hidden" name="profileForm" value="true">
                    <button type="submit" class="btn btn-info" id="submitBtn">Update</button>
                    <a href="{{SITE_URL}}dashboard" class="btn btn-default" role="button">Cancel</a>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</section>
@stop

@section('pagespecificscripts')
    <!-- flot charts scripts-->
    <script src="{{asset('dist/js/userSpecial.js')}}"></script>
@stop