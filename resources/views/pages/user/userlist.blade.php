@extends('layouts.master')

@section('title', 'User List')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <!-- <h1></h1> -->
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">User</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->

<section class="content">
    <div class="row">
        <div class="col-12">
            <div style=""><div id="divError" class="alert alert-danger" style="display:none"> <button type="button"  class="close" onclick="$(this).parent().hide();">×</button></div></div>
            @if ($errors->has('update'))
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ $errors->first('update') }}                            </div>
            @endif
            @if ($errors->has('useradd'))
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ $errors->first('useradd') }}</div>
            @endif
            <div class="card card-info">
                <div class="card-header">
                    <span class="float-sm-left">Users</span>
                    <span class="float-sm-right">
                        <a href="{{SITE_URL}}adduser">+ Add New</a>
                    </span>
                </div>
                <div class="card-body user-card-body">
                    <table id="data" class="border-none c-table-width table table-bordered table-striped table-responsive">
                        <thead>
                            <tr>
<!--                                <th width="5%">Id</th>-->
                                <th width="15%">Name</th>
                                <th width="18%">Email</th>
                                <th width="11%">Phone</th>
                                <th width="17%">Created At</th>
                                <th width="14%">User Role</th>
                                <th width="15%">Status</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('pagespecificscripts')
    <!-- flot charts scripts-->
    <script src="{{asset('dist/js/userSpecial.js')}}"></script>
@stop