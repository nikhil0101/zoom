<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use App\Models\Schooldetails;
use Illuminate\Http\Request;
use Validator;
use Redirect;
use Auth;
use DB;
use Hash;

class SchooldetailController extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    function index() {
        return View('pages/schooldetails/index');
    }

    public function create() {
        return view('pages/schooldetails/create');
    }

    public function store(Request $request) {
        $input = Input::all();
        // print_r($input);die;
        $rules = array(
            'school_name' => 'required',
            'school_contact' => 'required',
            'phone_number' => 'required',
            'school_address' => 'required',
            'image' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the validation
        if ($validator->fails()) {
            return Redirect::to('/schoolcreate')->withErrors($validator->messages());
        } else {

            $image = $request->file('image');
            $image->getClientOriginalExtension();
            $image = $request->file('image');
            $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('\images');
            $image->move($destinationPath, $input['imagename']);
            
            //echo '<pre>';print_r($input);die;
            $scl_detail = new Schooldetails;
            $scl_detail->school_name = $input["school_name"];
            $scl_detail->school_contact = $input["school_contact"];
            $scl_detail->phone_number = $input["phone_number"];
            $scl_detail->school_address = $input["school_address"];
            $scl_detail->school_logo = $input['imagename'];
            $scl_detail->save();

            return Redirect('schoollist')->withErrors(['details' => 'School details added Sucessfully.',]);
        }
    }

    public function destroy($id) {
        $DeleteSchool = DB::table('tbl_school_details')->where('id', $id)->delete();
    }

    public function editschooldetails($id) {
        $schooldetail = Schooldetails::findOrFail($id);
        return view('pages.schooldetails.edit', compact('schooldetail'));
    }

    public function update(Request $request) {
        $input = Input::all();

        $rules = array(
            'school_name' => 'required',
            'school_contact' => 'required',
            'phone_number' => 'required',
            'school_address' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the validation
        if ($validator->fails()) {

            return Redirect::to('/schoolcreate')->withErrors($validator->messages());
        } else {
            if(!empty($request->file('image'))){
                $image = $request->file('image');
                $image->getClientOriginalExtension();
                $image = $request->file('image');
                $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('\images');
                $image->move($destinationPath, $input['imagename']);
                $image_name = $input['imagename'];
            }else{
                $image_name = $input['oldImage'];
            }
            $id = $input['id'];
            $Name = $input['school_name'];
            $Contact = $input['school_contact'];
            $Number = $input['phone_number'];
            $Address = $input['school_address'];
            $Image = $image_name;

            DB::table('tbl_school_details')
                    ->where('id', $id)
                    ->update([ 'school_name' => "$Name", 'school_contact' => "$Contact", 'phone_number' => "$Number", 'school_address' => "$Address", 'school_logo' => "$Image"]);
            return Redirect('/schoollist')->withErrors(['update' => 'Details Update Sucessfully.',]);
        }
    }

    function ajaxlist(Request $request) {
        $columns = array(
            //0 => 'id',
            0 => 'school_name',
            1 => 'school_contact',
            2 => 'phone_number',
            3 => 'school_address',
            4 => 'school_logo',
            5 => 'action',
        );
        $totalData = Schooldetails::count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = Schooldetails::offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();
        } else {
            $search = $request->input('search.value');
            DB::enableQueryLog();
            $posts = Schooldetails::where('id', 'LIKE', "%{$search}%")
                    ->orWhere('school_name', 'LIKE', "%{$search}%")
                    ->orWhere('school_contact', 'LIKE', "%{$search}%")
                    ->orWhere('phone_number', 'LIKE', "%{$search}%")
                    ->orWhere('school_address', 'LIKE', "%{$search}%")
                    ->orWhere('school_logo', 'LIKE', "%{$search}%")
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();
            $totalFiltered = Schooldetails::where('id', 'LIKE', "%{$search}%")
                    ->orWhere('school_name', 'LIKE', "%{$search}%")
                    ->orWhere('school_contact', 'LIKE', "%{$search}%")
                    ->orWhere('phone_number', 'LIKE', "%{$search}%")
                    ->orWhere('school_address', 'LIKE', "%{$search}%")
                    ->orWhere('school_logo', 'LIKE', "%{$search}%")
                    ->count();
        }
        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $Editlink = route('editschooldetail', $post->id);
                //$Add = route('adduser');
                //$nestedData['id'] = $post->id;
                $nestedData['school_name'] = $post->school_name;
                $nestedData['school_contact'] = $post->school_contact;
                $nestedData['phone_number'] = $post->phone_number;
                $nestedData['school_address'] = $post->school_address;
//                $nestedData['school_logo'] = $post->school_logo;
                $nestedData['school_logo'] = "<img src='" . asset('/images/' . $post->school_logo) . "' height='50px' width='50px'>";
                $nestedData['action'] = "<a href='" . $Editlink . "'  title='Edit' class='btn btn-success' ><span class='fa fa-edit'></span></a> <button type='button' class='btn btn-danger' onclick='getSclConfirmation($post->id);'><i class='fa fa-trash'></i></button>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

}
