<!DOCTYPE html>
<style>
    .non-found-page{
        min-height: 100vh;
        display: flex;
        align-items: center;
        justify-content: center;
    }
</style>
<html>
    <head>
        <title>Page not Found</title>
        @include('includes.head')
    </head>
    <body class="hold-transition login-page">
        <div class="non-found-page">
            <div class="">
                <p style="font-size: 70px;flex-direction: row; justify-content:center; align-items:center;">404 Page not Found</p>
            </div>
        </div>
        <!-- /.login-box -->
        <!-- jQuery -->
        <script src="{{SITE_URL.'plugins/jquery/jquery.min.js' }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="{{SITE_URL.'plugins/bootstrap/js/bootstrap.bundle.min.js' }}"></script>
        <!-- iCheck -->
        <script src="{{SITE_URL.'plugins/iCheck/icheck.min.js' }}"></script>
        <script>
$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    })
})
        </script>
        <script>
            jQuery(document).ready(function () {
                jQuery('#myLoginform').validate({
                    rules: {
                        email: {
                            required: true,
                        },
                        password: {
                            required: true,
                        }
                    },
                    messages: {
                        email: {
                            required: "Email should not be blank.",
                            email: "Please enter valid email id."
                        },
                        password: {
                            required: "Password should not be blank.",
                        }
                    },
                });
            });
        </script>
    </body>
</html>
