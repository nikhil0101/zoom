<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use Hash;
use DB;
use Mail;

class ResetPasswordController extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    function ShowForgotPassword() {
        return View('pages/login/forgotpassword');
    }

    function ResetPassword($token = null) {

        $users = DB::table('tbl_user')
                ->where('forgot_token', $token)
//                ->where('expiry_date', '>', time())
                ->first();
        if ($users) {
            if ($token === $users->forgot_token) {
                return View('pages/login/resetpassword')->with('token', $token);
            }
        } else {
            return Redirect('/pagenotfound');
        }
    }

    function UpdatePassword(Request $request) {
        $input = $request->all();
        $token = $input['token'];
        $password = $input['password'];
        DB::table('tbl_user')
                ->where('forgot_token', $token)
                ->update(['password' => bcrypt($password), 'forgot_token' => ""]);

        return Redirect('/')->withErrors([
                    'sucess' => 'Changed Password Sucessfully please login.',
        ]);
    }

    /*
      |--------------------------------------------------------------------------
      | Function to the check email address exit or not in database
      |--------------------------------------------------------------------------
     */

    function CheckEmail() {
        $user = User::where('email', '=', Input::get('email'))->first();
        if ($user === null) {
            return Redirect('/forgotpassword')->withErrors([
                        'invalidpassword' => 'Invalid Email Address',
            ]);
        } else {
            $token = str_random(64);
            $timeout = time() + 24 * 60 * 60;
            $user->forgot_token = $token;
            $user->expiry_date = $timeout;
            $user->save();
            //=============Send Email========================       
            $userEmail = $user["email"];
            $Name = $user["full_name"];
            $token = $user["forgot_token"];
            $url = SITE_URL . "resetpssword/" . $token;
            $data = array('name' => $Name, 'token' => $url, "body" => "You are receiving this email because we received a password reset request for your account.");
            Mail::send('emails.payment', $data, function($message) use ($userEmail) {
                $message->to($userEmail, 'Artisans Web')
                        ->subject('Reset Password');
                $message->from('Admin@gmail.com', 'Admin');
            });
            //==============Send Email Over=======================
            return Redirect('/forgotpassword')->withErrors([
                        'checkemail' => 'Please Check your email to reset your password.',
            ]);
        }
    }

}
