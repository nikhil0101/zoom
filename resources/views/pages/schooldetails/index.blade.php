@extends('layouts.master')
@section('title', 'sample')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
<!--                <h1>School Details Table</h1>-->
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">School Details</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div style=""><div id="divError" class="alert alert-danger" style="display:none"> <button type="button"  class="close" onclick="$(this).parent().hide();">×</button></div></div>
            @if ($errors->has('update'))
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ $errors->first('update') }}                            </div>
            @endif
            @if ($errors->has('details'))
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ $errors->first('details') }}</div>
            @endif
            <div class="card card-info">
                <div class="card-header">
                        <span class="float-sm-left">School Details</span>
                        <span class="float-sm-right">
                            <a href="{{SITE_URL}}schoolcreate">+ Add New</a>
                        </span>
                    </div>
                <div class="card-body user-card-body">
                    <table id="schooldata" class="border-none c-table-width table table-bordered table-striped table-responsive">
                       
                        <thead>
                            <tr>
<!--                                <th width="5%">Id</th>-->
                                <th>School Name</th>
                                <th>Contact</th>
                                <th>Number</th>
                                <th>Address</th>
                                <th>Logo</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop


@section('pagespecificscripts')
    <!-- flot charts scripts-->
    <script src="{{asset('js/schooldetails.js')}}"></script>
@stop