@extends('layouts.master')

@section('title', 'sample')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <!-- <h1></h1> -->
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Events</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content -->
    <section class="content">
        @if(Session::has('message'))
            <div class="alert {{ Session::get('alert-class', 'alert-info') }}">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ Session::get('message') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12">
<div style=""><div id="divError" class="alert alert-danger" style="display:none"> <button type="button"  class="close" onclick="$(this).parent().hide();">×</button></div></div>
                <div class="card card-info">
                    <div class="card-header">
                        <span class="float-sm-left">Events</span>
                        <span class="float-sm-right">
                            <a href="{{SITE_URL}}events/create">+ Add New</a>
                        </span>
                    </div>
                    <div class="card-body user-card-body">
                        <table id="data" class="border-none c-table-width table table-bordered table-striped table-responsive dataTable no-footer">
                            <thead>
                                <tr>
<!--                                    <th width="5%">Id</th>-->
                                    <th width="15%">Title</th>
                                    <th width="20%">Description</th>
                                    <th width="20%">Start Date & Time</th>
                                    <th width="10%">Home Team</th>
                                    <th width="10%">Away Team</th>
                                    <th width="10%">Sports</th>
                                    <th width="15%">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('pagespecificscripts')
    <!-- flot charts scripts-->
    <script src="{{asset('js/main.js')}}"></script>
@stop