@extends('layouts.master')

@section('title', 'Add Event')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">    
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
<!--                    <h1>Add New Event</h1>-->
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Add Event</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="col-12">
                @if ($errors->has('addsuccessfully'))
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ $errors->first('addsuccessfully') }}
                </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="card card-info">
                    <div class="card-header">
                        <span class="float-sm-left">Add Event</span>
                    </div>
                    <div class="card-body register-card-body">
                        <!-- <p class="login-box-msg">Please add event details</p> -->
                        {{ Form::open(array('url' => 'events','id' => 'myEventform', 'name' => 'eventForm', 'method' => 'post')) }}
                            @if ($errors->has('update'))
                            <div class="alert alert-danger">
                                {{ $errors->first('update') }}
                            </div>
                            @endif
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Title</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="event_title" placeholder="Event Title">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Description</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="3" name="event_desc" placeholder="Event Description"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Start Date</label>
                                <div class="col-sm-10 input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                    <input type="text" class="form-control float-right" name="event_date" id="datepicker" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Start Time</label>
                                <div class="col-sm-10 input-group bootstrap-timepicker timepicker">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                    <input type="text" class="form-control float-right" name="event_time" id="timepicker1" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Home Team</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="home_team" name="home_team" placeholder="Home Team">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Away Team</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="away_team" name="away_team" placeholder="Away Team">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Sports</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="sports" id="sports">
                                        <option value="">Select any one</option>
                                        <option value="boy">Boys</option>
                                        <option value="girl">Girls</option>
                                        <option value="other">Other</option>
                                    </select>
                                </div>
                            </div>

                            <div class="sportsDetails"></div>
                            
                            <div class="sportsTable hide">
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Home Team :</label>
                                    <label class="col-sm-10 col-form-label" id="home_team_name"></label>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Visitor Team :</label>
                                    <label class="col-sm-10 col-form-label" id="away_team_name"></label>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Score by Quarters :</label>
                                </div>
                                <table class="table table-bordered">
                                <thead>
                                  <tr>
                                    <th>Team</th>
                                    <th>1Q</th>
                                    <th>2Q</th>
                                    <th>3Q</th>
                                    <th>4Q</th>
                                    <th>OT(all)</th>
                                    <th>F(Final)</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <th>Home Team</th>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                  </tr>
                                  <tr>
                                    <th>Visitor Team</th>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                        </div>

                        <div class="card-footer">
                              <button type="submit" class="btn btn-info">Add</button>
                              <a href="{{SITE_URL}}events" class="btn btn-default" role="button">Cancel</a>
                        </div>
                        {{ Form::close() }}
                    
                </div>
            </div>
        </div>
    </section>
@stop

@section('pagespecificscripts')
    <!-- flot charts scripts-->
    <script src="{{asset('js/main.js')}}"></script>
@stop