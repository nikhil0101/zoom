<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use App\Models\User;
use Validator;
use Auth;
use Hash;

class RegisterController extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    function showRagister() {
        return View('pages/login/register');
    }

    function InsertRagisterData() {
        $rules = array(
            'email' => 'required|email|unique:tbl_user'
        );
        $validator = Validator::make(Input::all(), $rules);
        $input = Input::all();
        if ($validator->fails()) {
            return Redirect('register')->withErrors([
                        'duplicatepassword' => 'This email address already exists please choose a different one',
            ]);
        } else {
            $signup_user = new User;
            $signup_user->full_name = $input["fullname"];
            $signup_user->email = $input["email"];
            $signup_user->password = bcrypt($input["password"]);
            $signup_user->save();
//            return Redirect('/');
            return Redirect('/')->withErrors([
                        'ragistersucess' => 'Ragistration sucessfully please login.',
            ]);
        }
    }

}
