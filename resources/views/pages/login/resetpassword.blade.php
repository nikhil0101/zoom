<!DOCTYPE html>
<html>
    <head>
        <title>Reset Password</title>
        @include('includes.head')
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="#"><b></b>Reset Password</a>
            </div>
            <div class="card">
                <div class="card-body login-card-body">
                    <p class="login-box-msg">Please enter your new password</p>
                    <form action="{{SITE_URL}}resetpssword" id="myLoginform" method="post">
                        @csrf
                        @if ($errors->has('sucess'))
                        <div class="alert alert-danger">
                            {{ $errors->first('sucess') }}
                        </div>
                        @endif
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" name="password" placeholder="Password">
                            <input type="hidden" class="form-control" name="token" value="<?php echo $token ?>" placeholder="">
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="checkbox icheck">
                                    <!--              <label>
                                                    <input type="checkbox"> Remember Me
                                                  </label>-->
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-6">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Change Password</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>

                </div>
                <!-- /.login-card-body -->
            </div>
        </div>
        <!-- /.login-box -->
        <!-- jQuery -->
        <script src="{{SITE_URL.'plugins/jquery/jquery.min.js' }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="{{SITE_URL.'plugins/bootstrap/js/bootstrap.bundle.min.js' }}"></script>
        <!-- iCheck -->
        <script src="{{SITE_URL.'plugins/iCheck/icheck.min.js' }}"></script>
        <script>
$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    })
})
        </script>
        <script>
            jQuery(document).ready(function () {
                jQuery('#myLoginform').validate({
                    rules: {
                        password: {
                            required: true,
                        }
                    },
                    messages: {
                        password: {
                            required: "Password should not be blank.",
                        }
                    },
                });
            });
        </script>
    </body>
</html>
