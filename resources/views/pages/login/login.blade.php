<style>
    .bg-img {
        width: 100%;
       // height: 400px;
        background-image: url("{{SITE_URL.'plugins/img/login.jpg'}}")!important;
        background-repeat: no-repeat;
        background-size: contain;
       // border: 1px solid red;
    }
</style>
<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        @include('includes.head')
    </head>
    <body class="hold-transition login-page bg-img">
        <div class="login-box">
            <div class="login-logo">
                <a href="#"><b></b>.</a>
            </div>
            <!-- /.login-logo -->
            <div class="card">
                <div class="card-body login-card-body">
                    <p class="login-box-msg">Welcome, Please Login
                    </p>
                    <form action="{{SITE_URL}}login" id="myLoginform" method="post">
                        @csrf
                        @if ($errors->has('invalidpassword'))
                        <div class="alert alert-danger">
                            {{ $errors->first('invalidpassword') }}
                        </div>
                        @endif
                        @if ($errors->has('sucess'))
                        <div class="alert alert-success">
                            {{ $errors->first('sucess') }}
                        </div>
                        @endif
                        @if ($errors->has('invaliuser'))
                        <div class="alert alert-danger">
                            {{ $errors->first('invaliuser') }}
                        </div>
                        @endif
                        @if ($errors->has('ragistersucess'))
                        <div class="alert alert-success">
                            {{ $errors->first('ragistersucess') }}
                        </div>
                        @endif
                        <div class="form-group has-feedback">
                            <input type="email" class="form-control" name="email" placeholder="Email">

<!--          <span class="fa fa-envelope form-control-feedback"></span>-->
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" name="password" placeholder="Password">
                    <!--          <span class="fa fa-lock form-control-feedback"></span>-->
                        </div>
                        <div class="row">
                            <div class="col-8">
                                <div class="checkbox icheck">
                                    <!--              <label>
                                                    <input type="checkbox"> Remember Me
                                                  </label>-->
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                    <p class="mb-1">
                        <a href="{{SITE_URL}}forgotpassword" class="custom_display_msg">Forgot password ?</a>
                    </p>
<!--                    <p class="mb-0">
                        Don't have an account ? <a href="{{SITE_URL}}register" class="text-center custom_display_msg">Sign up</a>
                    </p>-->
                </div>
                <!-- /.login-card-body -->
            </div>
        </div>
        <!-- /.login-box -->
        <!-- jQuery -->
        <script src="{{SITE_URL.'plugins/jquery/jquery.min.js' }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="{{SITE_URL.'plugins/bootstrap/js/bootstrap.bundle.min.js' }}"></script>
        <!--         iCheck 
                <script src="{{SITE_URL.'plugins/iCheck/icheck.min.js' }}"></script>-->
        <script>
$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    })
})
        </script>
        <script>
            jQuery(document).ready(function () {
                jQuery('#myLoginform').validate({
                    rules: {
                        email: {
                            required: true,
                        },
                        password: {
                            required: true,
                        }
                    },
                    messages: {
                        email: {
                            required: "Email should not be blank.",
                            email: "Please enter valid email id."
                        },
                        password: {
                            required: "Password should not be blank.",
                        }
                    },
                    errorPlacement: function () {
                        return false;  // suppresses error message text
                    }
                });
            });
        </script>
    </body>
</html>
