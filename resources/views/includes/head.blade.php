<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Font Awesome -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{SITE_URL.'plugins/daterangepicker/daterangepicker-bs3.css'}}">
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
<!-- Theme style -->
<link rel="stylesheet" href="{{SITE_URL.'dist/css/adminlte.min.css'}}">
<!-- iCheck -->
<link rel="stylesheet" href="{{SITE_URL.'plugins/iCheck/flat/blue.css'}}">
<!-- Morris chart -->
<link rel="stylesheet" href="{{SITE_URL.'plugins/morris/morris.css'}}">

<!-- jvectormap -->
<link rel="stylesheet" href="{{SITE_URL.'plugins/jvectormap/jquery-jvectormap-1.2.2.css'}}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{SITE_URL.'plugins/datepicker/datepicker3.css'}}">
<!-- Time Picker -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap.timepicker/0.2.6/css/bootstrap-timepicker.min.css">

<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{SITE_URL.'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'}}">
<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
<!-- IonIcons -->
<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="{{SITE_URL.'dist/css/custom.css'}}">


<script type="text/javascript">
	var baseUrl = "{{ SITE_URL }}";
    var csrf_token = "{{csrf_token()}}";
</script>