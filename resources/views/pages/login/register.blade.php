<!DOCTYPE html>
<html>
    <head>
        <title>Register</title>
        @include('includes.head')
    </head>
    <body class="hold-transition register-page">
        <div class="register-box">
            <div class="register-logo">
                <a href="#"><b></b>Register From</a>
            </div>
            <div class="card">
                <div class="card-body register-card-body">
                    <p class="login-box-msg">Please Complete your details</p>

                    <form action="{{SITE_URL}}register" id="MyRegisterForm" method="post">
                        @csrf
                        @if ($errors->has('duplicatepassword'))
                        <div class="alert alert-danger">
                            {{ $errors->first('duplicatepassword') }}
                        </div>
                        @endif
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" name="fullname" placeholder="Full name">
<!--                            <span class="fa fa-user form-control-feedback"></span>-->
                        </div>
                        <div class="form-group has-feedback">
                            <input type="email" class="form-control" name="email" placeholder="Email">
<!--                            <span class="fa fa-envelope form-control-feedback"></span>-->
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" name="password" placeholder="Password">
<!--                            <span class="fa fa-lock form-control-feedback"></span>-->
                        </div>
                        <!--                        <div class="form-group has-feedback">
                                                    <input type="password" class="form-control" placeholder="Retype password">
                                                    <span class="fa fa-lock form-control-feedback"></span>
                                                </div>-->
                        <div class="row">
                            <div class="col-8">
                                <!--                                <div class="checkbox icheck">
                                                                    <label>
                                                                        <input type="checkbox"> I agree to the <a href="#">terms</a>
                                                                    </label>
                                                                </div>-->
                            </div>
                            <!-- /.col -->
                            <div class="col-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                    <!--                    <div class="social-auth-links text-center">
                                            <p>- OR -</p>
                                            <a href="#" class="btn btn-block btn-primary">
                                                <i class="fa fa-facebook mr-2"></i>
                                                Sign up using Facebook
                                            </a>
                                            <a href="#" class="btn btn-block btn-danger">
                                                <i class="fa fa-google-plus mr-2"></i>
                                                Sign up using Google+
                                            </a>
                                        </div>-->
                    <a href="{{SITE_URL}}" class="text-center">I already have a membership</a>
                </div>
                <!-- /.form-box -->
            </div><!-- /.card -->
        </div>
        <!-- /.register-box -->
        <!-- jQuery -->
        <script src="{{SITE_URL.'plugins/jquery/jquery.min.js' }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="{{SITE_URL.'plugins/bootstrap/js/bootstrap.bundle.min.js' }}"></script>
        <!-- iCheck -->
        <script src="{{SITE_URL.'plugins/iCheck/icheck.min.js' }}"></script>
        <script>
$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    })
})
        </script>
        <script>
            jQuery(document).ready(function () {
                jQuery('#MyRegisterForm').validate({
                    rules: {
                        fullname: {
                            required: true,
                        },
                        email: {
                            required: true,
                        },
                        password: {
                            required: true,
                        }
                    },
                    messages: {
                        fullname: {
                            required: "Name should not be blank.",
                        },
                        email: {
                            required: "Email should not be blank.",
                            email: "Please enter valid email id."
                        },
                        password: {
                            required: "Password should not be blank.",
                        }
                    },
                });
            });
        </script>
    </body>
</html>
