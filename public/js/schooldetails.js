//======================School Details Start=============================
jQuery(document).ready(function () {
    var details_url = baseUrl + 'school_detail_list';

    jQuery('#schooldata').DataTable({
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "lengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
        "ajax": {
            "url": details_url,
            "dataType": "json",
            "type": "POST",
            "data": {'_token': csrf_token}
        },
        "columns": [
//            {"data": "id"},
            {"data": "school_name"},
            {"data": "school_contact"},
            {"data": "phone_number"},
            {"data": "school_address"},
            {"data": "school_logo"},
            {"data": "action"}
        ],
        aoColumnDefs: [
        {
           bSortable: false,
           aTargets: [ -1, -2 ]
        }
      ]
    });
    setTimeout(function () {
        jQuery('.alert').fadeOut(500)
    }, 2000);
});

$('#schoolform').validate({
    rules: {
        school_name: {
            required: true,
        },
//        school_contact: {
//            required: true,
//            number: true,
//            minlength: 9,
//            maxlength: 10
//        },
        phone_number: {
            required: true,
            number: true,
            minlength: 9,
            maxlength: 10
        },
        school_address: {
            required: true,
        },
    },
    messages: {
    },
    errorPlacement: function () {
        return false;  // suppresses error message text
    }
});



function getSclConfirmation($id) {
    var delete_url = baseUrl + 'deletesclinfo/';
    var retVal = confirm("Are you sure?");
    if (retVal == true) {
        var url_id = $id;
        $.ajax({
            type: "GET",
            url: delete_url + url_id,
            data: {_token: csrf_token},
            success: function (data) {
                $('#divError').html('School detail deleted successfully. <button type="button" onclick="$(this).parent().hide();" class="close" >Ã—</button>');
                $('#divError').removeClass('alert-danger');
                $('#divError').addClass('alert-success');
                $('#divError').show(100);
                $('#divError').fadeOut(1000);
                $('#divError .close').show(500);
                $("html, body").animate({scrollTop: 0}, "fast");
                var table = $('#schooldata').DataTable();
                table.ajax.reload();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }
    else
    {
        //odcument.write("User does not want to continue!");
        return false;
    }
}

   