$(document).ready(function () {
    /* general element start */
    // set?
    /* general element stop */

    /* event page start */
    var url = baseUrl + 'alleventlist';
    $('#data').DataTable({
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "lengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
        "ajax": {
            "url": url,
            "dataType": "json",
            "type": "POST",
            "data": {'_token': csrf_token},
            "complete": function(){
                setTimeout(function(){
                    $('.alert').addClass('hide');
                }, 3000);
            }
        },
        "columns": [
//            {"data": "id"},
            {"data": "event_title"},
            {"data": "event_description"},
            {"data": "event_start_date_time"},
            {"data": "home_team"},
            {"data": "away_team"},
            {"data": "sports"},
            {"data": "action"},
        ],
        "columnDefs": [
           { "orderable": false, "targets": -1 }
        ]

    });

    $('#myEventform').validate({
        rules: {
            event_title: {
                required: true,
            },
            event_date: {
                required: true,
            },
            time_hours: {
                required: true,
            },
            time_minute: {
                required: true,
            },
            home_team: {
                required: true,
            },
            away_team: {
                required: true,
            },
            sports:{
                required: true,
            }

        },
        messages: {
        },
        errorPlacement: function(){
            return false;  // suppresses error message text
        }
    });

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      startDate: new Date()
    });

    $('#timepicker1').timepicker({
        showInputs: false
    });

    $(document).on('change', '#sports', function() {
        if(this.value != ''){
            $.ajax({
               type:'POST',
               url: "sportsList",
               data: {
                    '_token': $('input[name=_token]').val(),
                    'type': this.value
                },
               success:function(data){
                  if(data != false){
                    $('.sportsDetails').show().html(data);
                  }
               }
            });
        }else{
            $(".sportsDetails").hide();
            $('.sportsTable').addClass('hide');
        }
    });

    function capitalSubstring(string){
        myString = string;   // cheeseburger
        firstChar = myString.substring( 0, 3 ); // == "che"
        tail = myString.substring( 3 ); // == "eseburger"
        return myString = firstChar.toUpperCase() + tail; // myString == "Cheeseburger"
    }
    
    $(document).on('change', '#games', function() {
        if(this.value == 3){
            myStringHome = capitalSubstring($('#home_team').val());
            myStringAway = capitalSubstring($('#away_team').val());

            $('.sportsTable').removeClass('hide');
            $('#home_team_name').html(myStringHome);
            $('#away_team_name').html(myStringAway);
        }else{
            $('.sportsTable').addClass('hide');
        }
    });
    
    $(document).on('keyup', '#home_team', function(){
        myStringHome = capitalSubstring($(this).val());
        $('#home_team_name').html(myStringHome);
    });
    $(document).on('keyup', '#away_team', function(){
        myStringHome = capitalSubstring($(this).val());
        $('#away_team_name').html(myStringHome);
    });

    
    /* event page stop */

    
});


