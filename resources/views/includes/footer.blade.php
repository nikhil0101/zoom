<!-- /.content-wrapper -->
<footer class="main-footer">
    <strong>Copyright &copy; 2017-2018 <a href="#">Zoom</a>.</strong>
    All rights reserved.
</footer>
<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<!-- jQuery -->
<script src="{{SITE_URL.'plugins/jquery/jquery.min.js' }}"></script>
<script src="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<script src="https://cdn.jsdelivr.net/bootstrap.timepicker/0.2.6/js/bootstrap-timepicker.min.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>-->
<!-- jQuery UI 1.11.4 -->
<script src="{{SITE_URL.'plugins/jquery/jquery-ui.min.js' }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
$.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{SITE_URL.'plugins/bootstrap/js/bootstrap.bundle.min.js' }}"></script>
<script src="{{SITE_URL.'plugins/select2/select2.full.min.js' }}"></script>

<!-- InputMask -->
<script src="{{SITE_URL.'plugins/input-mask/jquery.inputmask.js' }}"></script>
<script src="{{SITE_URL.'plugins/input-mask/jquery.inputmask.date.extensions.js' }}"></script>
<script src="{{SITE_URL.'plugins/input-mask/jquery.inputmask.extensions.js' }}"></script>

<!-- DataTables -->
<!--<script src="{{SITE_URL.'plugins/datatables/jquery.dataTables.min.js' }}"></script>
<script src="{{SITE_URL.'plugins/datatables/dataTables.bootstrap4.min.js' }}"></script>-->
<script type="text/javascript" charset="utf8" src="{{SITE_URL.'plugins/jquery/jquery.dataTables.js' }}"></script>
<!-- Morris.js charts -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
<!-- Sparkline -->
<script src="{{SITE_URL.'plugins/sparkline/jquery.sparkline.min.js' }}"></script>
<!-- jvectormap -->
<script src="{{SITE_URL.'plugins/jvectormap/jquery-jvectormap-1.2.2.min.js' }}"></script>
<script src="{{SITE_URL.'plugins/jvectormap/jquery-jvectormap-world-mill-en.js' }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{SITE_URL.'plugins/knob/jquery.knob.js' }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="{{SITE_URL.'plugins/daterangepicker/daterangepicker.js' }}"></script>


<!-- datepicker -->
<script src="{{SITE_URL.'plugins/datepicker/bootstrap-datepicker.js' }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{SITE_URL.'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js' }}"></script>
<!-- Slimscroll -->
<script src="{{SITE_URL.'plugins/slimScroll/jquery.slimscroll.min.js' }}"></script>
<!-- FastClick -->
<script src="{{SITE_URL.'plugins/fastclick/fastclick.js' }}"></script>
<!-- AdminLTE App -->
<script src="{{SITE_URL.'dist/js/adminlte.js' }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{SITE_URL.'dist/js/pages/dashboard.js' }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{SITE_URL.'dist/js/demo.js' }}"></script>

<!--<script src="{{SITE_URL.'plugins/jquery/jquery.min.js' }}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>

<script type="text/javascript">
$('<input type="text" />').appendTo(container)).timepicker({
    buttonImage: "icon url",
    buttonImageOnly: true,
    showOn: 'button'
});
</script>

<script>
$("#timepicker1").click(function () {
    $('body').find('.icon-chevron-up').each(function (  ) {
        $(this).addClass('fa fa-angle-up');
    });
    $('body').find('.icon-chevron-down').each(function (  ) {
        $(this).addClass('fa fa-angle-down');
    });
});
</script>

<!-- page specific scripts -->
@yield('pagespecificscripts');