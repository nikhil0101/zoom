@extends('layouts.master')

@section('title', 'Edit User')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">    
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <!-- <h1></h1> -->
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Update User</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="col-12">
            @if ($errors->has('deletesucess'))
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong>{{ $errors->first('deletesucess') }}                            </div>
            @endif
            @if ($errors->has('duplicate_email'))
            <div class="alert alert-danger">
                {{ $errors->first('duplicate_email') }}
            </div>
            @endif
            <div class="card card-info">
                <div class="card-header">
                    <span class="float-sm-left">Update User</span>
                </div>
                <div class="card-body register-card-body">
                    <!-- <p class="login-box-msg">Please add event details</p> -->
                    {{ Form::open(array('url' => 'updateuser','id' => 'myEditform', 'name' => '', 'method' => 'post')) }}
                    @csrf

                    <div class="form-group has-feedback">
                        <input type="hidden" class="form-control" name="id" value="{{ $user->id }}"placeholder="Id">
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-1 col-form-label">Full Name</label>
                        <div class="col-sm-11">
                            <input type="text" class="form-control" name="fullname" value="{{ $user->full_name }}" placeholder="Full name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-1 col-form-label">Email</label>
                        <div class="col-sm-11">
                            <input type="email" class="form-control" name="email" value="{{ $user->email }}"  placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-1 col-form-label">Phone</label>
                        <div class="col-sm-11">
                            <input type="text" class="form-control" name="phone" value="{{ $user->phone }}"  placeholder="Phone">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-1 col-form-label">Role</label>
                        <div class="col-sm-11">
                            <select  name="user_roll" id="user_roll" class="form-control">
                                <option value="">Select Role</option>
                                @if(Auth::user()->user_roll == 1)
                                    <option value="2" {{ $user->user_roll == 2 ? 'selected="selected"' : '' }}>Platform Admin</option>
                                @endif
                                <option value="3" {{ $user->user_roll == 3 ? 'selected="selected"' : '' }}>School Admin</option>
                                <option value="4" {{ $user->user_roll == 4 ? 'selected="selected"' : '' }}>End user</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-info">Update</button>
                    <a href="{{SITE_URL}}userlist" class="btn btn-default" role="button">Cancel</a>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</section>
@stop

@section('pagespecificscripts')
<!-- flot charts scripts-->
<script src="{{asset('dist/js/userSpecial.js')}}"></script>
@stop   