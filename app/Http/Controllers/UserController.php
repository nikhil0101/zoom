<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use App\Models\User;
use Validator;
use Auth;
use Hash;
use Redirect;
use DB;
use Illuminate\Http\Request;
use App\Models\Userlist;

class UserController extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public function ShowUserList() {
        return View('pages/user/userlist');
    }

    public function ShowUserEditList() {
        return View('pages/user/edituser');
    }

    public function ShowAddUser() {
        return View('pages/user/adduser');
    }

    public function AllUserList(Request $request) {
        $columns = array(
            0 => 'id',
            1 => 'full_name',
            2 => 'email',
            3 => 'phone',
            4 => 'created_at',
            5 => 'status',
            6 => 'action',
        );
        $loginUserId = Auth::user()->id;
        $totalData = Userlist::with("Role")
                ->where('status', '!=', '2')
                ->where('user_roll', '!=', '1')
                ->where('id', '!=', $loginUserId)
                ->where('created_by', '=', $loginUserId)
                ->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = Userlist::with("Role")
                    ->offset($start)
                    ->where('status', '!=', '2')
                    ->where('user_roll', '!=', '1')
                    ->where('id', '!=', $loginUserId)
                    ->where('created_by', '=', $loginUserId)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();
        } else {
            $search = $request->input('search.value');
            DB::enableQueryLog();
            $posts = Userlist::
                    where(function($q)use($search) {
                        $q->orWhere('full_name', 'LIKE', "%{$search}%");
                        $q->orWhere('email', 'LIKE', "%{$search}%");
                        $q->orWhere('phone', 'LIKE', "%{$search}%");
                        $q->orWhere('created_at', 'LIKE', "%{$search}%");
                        $q->orWhere('id', 'LIKE', "%{$search}%");
                        $q->orWhereHas('Role', function($qu)use($search) {
                            $qu->where('role_name', 'LIKE', "%{$search}%");
                        });
                    })
                    ->where('status', '!=', '2')
                    ->where('user_roll', '!=', '1')
                    ->where('id', '!=', $loginUserId)
                    ->where('created_by', '=', $loginUserId)
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();

            $totalFiltered = Userlist::
                    where(function($q)use($search) {
                        $q->orWhere('full_name', 'LIKE', "%{$search}%");
                        $q->orWhere('email', 'LIKE', "%{$search}%");
                        $q->orWhere('phone', 'LIKE', "%{$search}%");
                        $q->orWhere('created_at', 'LIKE', "%{$search}%");
                        $q->orWhere('id', 'LIKE', "%{$search}%");
                        $q->orWhereHas('Role', function($qu)use($search) {
                            $qu->where('role_name', 'LIKE', "%{$search}%");
                        });
                    })
                    ->where('status', '!=', '2')
                    ->where('user_roll', '!=', '1')
                    ->where('id', '!=', $loginUserId)
                    ->where('created_by', '=', $loginUserId)
                    ->count();
        }

        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $Editlink = route('edituser', $post->id);
                $nestedData['id'] = $post->id;
                $nestedData['full_name'] = $post->full_name;
                $nestedData['email'] = "<a href=mailto:'" . $post->email . "'>" . $post->email . "</a>";
                $nestedData['phone'] = $post->phone;
                $nestedData['created_at'] = date('m-d-Y h:i A', strtotime($post->created_at));
                $nestedData['user_roll'] = $post->Role->role_name;
                if ($post->status == 1) {
                    $nestedData['status'] = "<button type='button' class='btn btn-block btn-success btn-sm' onclick='ChangeStatus($post->id);'>Active</button>";
                } else {
                    $nestedData['status'] = "<button type='button' class='btn btn-block btn-danger btn-sm' onclick='ChangeStatus($post->id);'>Inactive</button>";
                }

                $nestedData['action'] = "<a href='" . $Editlink . "'  title='Edit' class='btn btn-success' ><span class='fa fa-edit'></span></a> <button type='button' class='btn btn-danger' onclick='getConfirmation($post->id);'><i class='fa fa-trash'></i></button>";

                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
        die;
    }

    public function DeteleUser($id) {
        DB::table('tbl_user')
                ->where('id', $id)
                ->update(['status' => '2']);
    }

    public function EditUser($id) {
        $user = Userlist::findOrFail($id);
        return view('pages.user.edituser', compact('user'));
    }

    public function ChangeStatus() {
        $input = Input::all();
        $id = $input['id'];
        $status = User::select('status')->where('id', $id)->first();
        if ($status->status == '1') {
            $checkstatus = '0';
        } else {
            $checkstatus = '1';
        }
        DB::table('tbl_user')
                ->where('id', $id)
                ->update([ 'status' => $checkstatus]);
    }

    public function AddUser() {
        // validate
        $rules = array(
            'fullname' => 'required',
            'email' => 'required|email',
            'phone' => 'required|min:10|numeric',
            'user_roll' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the validation
        if ($validator->fails()) {
            return Redirect::to('adduser')->withErrors($validator->messages());
        } else {
            $input = Input::all();
            $email = $input['email'];
            $checkEmail = DB::table('tbl_user')
                    ->where('email', $email)
                    ->where('status', '!=', '2')
                    ->count();

            if ($checkEmail > 0) {
                //return Redirect::to('/adduser')->withErrors($validator->messages());
                return Redirect('/adduser')->withErrors(['duplicate_email' => 'This email address already exists please choose a different one',]);
                //return Redirect('adduser')->withErrors($validator->messages(['duplicate_email' => 'This email address already exists please choose a different one']));
            } else {
                $input = Input::all();
                $signup_user = new User;
                $signup_user->full_name = $input["fullname"];
                $signup_user->email = $input["email"];
                $signup_user->phone = $input["phone"];
                if ($input["user_roll"] == '2') {
                    $pwd = 'platform@123';
                } elseif ($input["user_roll"] == '3') {
                    $pwd = 'school@123';
                } else {
                    $pwd = 'user@123';
                }
                $signup_user->password = bcrypt($pwd);
                $signup_user->user_roll = $input["user_roll"];
                $signup_user->created_by = Auth::user()->id;
                $signup_user->save();

                return Redirect('/userlist')->withErrors(['useradd' => 'User added Sucessfully',]);
            }
        }
    }

    public function UpdateUser(Request $request) {
        $input = Input::all();
        $id = $input['id'];
        $rules = array(
            'fullname' => 'required',
            'email' => 'required|email|unique:tbl_user,email,' . $id,
            'phone' => 'required|min:10|numeric',
            'user_roll' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
        // process the validation
        if ($validator->fails()) {
            if (!empty($input['profileForm']) && $input['profileForm'] == 'true') {
                $redirect = '/showprofile';
            } else {
                $redirect = '/edituser/' . $input['id'];
            }
            return Redirect($redirect)->withErrors(['duplicate_email' => 'This email address already exists please choose a different one',]);
        } else {
            $Address = '';
            $input = $request->all();
            $Name = $input['fullname'];
            $Email = $input['email'];
            $Phone = $input['phone'];
            $Userroll = $input['user_roll'];
            if (!empty($input['address'])) {
                $Address = $input['address'];
            }
            if (!empty($input['profileForm']) && $input['profileForm'] == 'true') {
                $redirect = '/showprofile';
            } else {
                $redirect = '/userlist';
            }
            if (isset($input['id']) && !empty($input['id'])) {
                $id = $input['id'];
                DB::table('tbl_user')
                        ->where('id', $id)
                        ->update([ 'full_name' => "$Name", 'email' => "$Email", 'phone' => "$Phone", 'user_roll' => "$Userroll", 'address' => "$Address"]);
                return Redirect($redirect)->withErrors(['update' => 'User updated successfully',]);
            }
        }
    }

    function showprofile() {
        $user = auth()->user();
        $user = Userlist::with('Role')->findOrFail($user->id);
        //print_r($user);die;
        return view('pages.user.profile', compact('user'));
    }

}
