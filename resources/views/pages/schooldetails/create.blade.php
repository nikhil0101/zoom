@extends('layouts.master')
@section('title', 'sample')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">    
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
<!--                <h1>Add School Details</h1>-->
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Add School Details</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="col-12">
            @if ($errors->has('deletesucess'))
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong>{{ $errors->first('deletesucess') }}</div>
            @endif
            @if ($errors->has('duplicate_email'))
            <div class="alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ $errors->first('duplicate_email') }}</div>
            @endif
            
            @if ($errors->any())
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-info">
                <div class="card-header">
                    <span class="float-sm-left">Add School Details</span>
                </div>
                <div class="card-body register-card-body">
                    <!-- <p class="login-box-msg">Please add event details</p> -->
                    {{ Form::open(array('url' => 'addschoolinfo','id' => 'schoolform', 'name' => 'schoolinfo', 'method' => 'POST', 'files'=>true)) }}
                    @if ($errors->has('update'))
                    <div class="alert alert-danger">
                        {{ $errors->first('update') }}
                    </div>
                    @endif
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">School Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="school_name" placeholder="School name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">School Contact</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="school_contact"  placeholder="School Contact">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">School Number</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="phone_number"  placeholder="School Number">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">School Address</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="school_address"  placeholder="School Address">
                        </div>
                    </div>
                    <!--                    <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label">School Logo</label>
                                            <div class="col-sm-10">
                                                {!! Form::file('image', array('class' => 'image')) !!}
                                            </div>
                                        </div>-->
                    <div class="form-group row right-marg-0 file-label-detail">
                        <label for="staticEmail" class="col-sm-2 col-form-label">School Logo</label>
                        <div class="custom-file col-sm-10">
                            {!! Form::file('image', array('class' => 'custom-file-input', 'id' => 'exampleInputFile' )) !!}
                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-info">Add</button>
                    <a href="{{SITE_URL}}schoollist" class="btn btn-default" role="button">Cancel</a>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</section>
@stop

@section('pagespecificscripts')
<!-- flot charts scripts-->
<script src="{{asset('js/schooldetails.js')}}"></script>
@stop

