<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;
use Hash;
use Redirect;
use DB;
use Illuminate\Http\Request;
use App\Models\Eventlist;
use App\Models\sportsList;
use Session;

class EventController extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    function index() {
        return view('pages/event/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // load the create form (app/views/nerds/create.blade.php)
        return view('pages/event/addevent');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        // validate
        $rules = array(
            'event_title' => 'required',
            'event_date' => 'required',
            'event_time' => 'required',
            'home_team' => 'required',
            'away_team' => 'required',
            'sports' => 'required',
            'games' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
       
        // process the validation
        if ($validator->fails()) {
            return Redirect::to('events/create')->withErrors($validator->messages());
        } else {
            // store
            $request = Input::all();
            $addevent = new Eventlist;
            $addevent->event_title = $request["event_title"];
            if(!empty($request["event_desc"])){
                $addevent->event_description = $request["event_desc"];
            }
            $full_date = date('Y-m-d H:i:s', strtotime($request["event_date"] .' '. $request["event_time"]));
            $addevent->event_start_date_time = $full_date;
            $addevent->home_team = $request["home_team"];
            $addevent->away_team = $request["away_team"];
            $addevent->sports = $request["sports"];
            $addevent->games = $request["games"];
            $addevent->status = 1;
            $addevent->save();
            
            // redirect
            Session::flash('message', 'Event created sucessfully!');
            Session::flash('alert-class', 'alert-success');
            return Redirect::to('events');
        }
    }

    function DeteleUser($id) {
        DB::table('tbl_event')
                ->where('id', $id)
                ->update(['status' => '2']);
    }

    function AllEventList(Request $request) {

        $columns = array(
            //0 => 'id',
            0 => 'event_title',
            1 => 'event_description',
            2 => 'event_start_date_time',
            3 => 'home_team',
            4 => 'away_team',
            5 => 'sports',
            6 => 'action',
        );
        $totalData = Eventlist::count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = Eventlist::offset($start)
                    ->where('status', '!=', '2')
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();
        } else {
            $search = $request->input('search.value');
            DB::enableQueryLog();
            $posts = Eventlist::where('status', '!=', '2')
                    ->where(function($q)use($search) {
                        $q->orWhere('event_title', 'LIKE', "%{$search}%");
                        $q->orWhere('event_description', 'LIKE', "%{$search}%");
                        $q->orWhere('event_start_date_time', 'LIKE', "%{$search}%");
                        $q->orWhere('home_team', 'LIKE', "%{$search}%");
                        $q->orWhere('away_team', 'LIKE', "%{$search}%");
                        $q->orWhere('sports', 'LIKE', "%{$search}%");
                        $q->orWhere('id', 'LIKE', "%{$search}%");
                    })
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();
            $totalFiltered = Eventlist::where('id', 'LIKE', "%{$search}%")
                    ->where('status', '!=', '2')
                    ->orWhere('event_title', 'LIKE', "%{$search}%")
                    ->orWhere('event_description', 'LIKE', "%{$search}%")
                    ->orWhere('event_start_date_time', 'LIKE', "%{$search}%")
                    ->orWhere('home_team', 'LIKE', "%{$search}%")
                    ->orWhere('away_team', 'LIKE', "%{$search}%")
                    ->orWhere('sports', 'LIKE', "%{$search}%")
                    ->count();
        }
        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $Editlink = route('edituser', $post->id);
                //$Add = route('adduser');
                //$nestedData['id'] = $post->id;
                $nestedData['event_title'] = $post->event_title;
                $nestedData['event_description'] = $post->event_description;
                //$nestedData['event_start_date_time'] = $post->event_start_date_time;
                $nestedData['event_start_date_time'] = date('m-d-Y h:i A', strtotime($post->event_start_date_time));
                $nestedData['home_team'] = $post->home_team;
                $nestedData['away_team'] = $post->away_team;
                $nestedData['sports'] = $post->sports;
                $nestedData['action'] = "<a href='#'  title='Edit' class='btn btn-success' ><span class='fa fa-edit'></span></a> <button type='button' class='btn btn-danger' onclick='#'><i class='fa fa-trash'></i></button>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }


    public function sportsList(Request $request){
        $type = $request->input('type');
        if(!empty($type)){
            $sportsList = sportsList::where('sport_category', '=', $type)->get()->toArray();
            if(count($sportsList) > 0){
                $html = '<div class="form-group row">';
                $html .= '<label class="col-sm-2 col-form-label">Game</label>';
                $html .= '<div class="col-sm-10">';
                $html .= '<select class="form-control" name="games" id="games">';
                $html .= '<option value="">Select any one</option>';
                foreach ($sportsList as $key => $value) {
                    $html .= '<option value="'.$value['id'].'">'.$value['sport_name'].'</option>';
                }
                $html .= '</select>';
                $html .= '</div>';
                $html .= '</div>';
                return $html;
            }else{
                return false;
            }
        }else{
            return false;
        }

    }

}
