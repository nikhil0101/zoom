<nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" style="font-weight: bold;">
                <i class="fa fa-user"></i> <?php $user = auth()->user(); echo $user->full_name ?>
            </a>

            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <div class="dropdown-divider"></div>

                <a href="{{SITE_URL}}showprofile" class="nav-link">
                   
                    <p>
                        <i class="nav-icon fa fa-user"></i> My Profile
                    </p>
                </a>  
                <a href="{{SITE_URL}}/logout" class="nav-link">
                    <p>
                        <i class="nav-icon fa fa-sign-out"></i> Logout
                    </p>
                </a>
            </div>
        </li>
    </ul>
</nav>