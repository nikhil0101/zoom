<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class SportsList extends Model
{
     protected $table = "tbl_sport_list";
}