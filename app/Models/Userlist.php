<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Eloquent;

class Userlist extends Eloquent
{
     protected $table = "tbl_user";
     
    public function Role()
    {
       return $this->hasOne('App\Models\Userroll', 'id', 'user_roll')->select("id","role_name");
    }
}