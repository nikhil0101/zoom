<!DOCTYPE html>
<html>
    <head>
        <title>Forgot Password</title>
        @include('includes.head')
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="#"><b></b>Forgot your password?</a>
            </div>
            <!-- /.login-logo -->
            <div class="card">
                <div class="card-body login-card-body">
                    <p class="login-box-msg">Please enter the email address registered on your account.</p>
                    <form action="{{SITE_URL}}forgotpassword" id="myLoginform" method="post">

                        @csrf
                        @if ($errors->has('invalidpassword'))
                        <div class="alert alert-danger">
                            {{ $errors->first('invalidpassword') }}
                        </div>
                        @endif
                        @if ($errors->has('checkemail'))
                        <div class="alert alert-success">
                            {{ $errors->first('checkemail') }}
                        </div>
                        @endif
                        <div class="form-group has-feedback">
                            <input type="email" class="form-control" name="email" placeholder="Email">
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <button type="button" onclick="window.location='{{SITE_URL}}/'" class="btn btn-primary btn-block btn-flat">Go to Login</button>
                            </div>
                            <!-- /.col -->
                            <div class="col-6">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Reset Password</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                </div>
                <!-- /.login-card-body -->
            </div>
        </div>
        <!-- /.login-box -->
        <!-- jQuery -->
        <script src="{{SITE_URL.'plugins/jquery/jquery.min.js' }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="{{SITE_URL.'plugins/bootstrap/js/bootstrap.bundle.min.js' }}"></script>
        <!-- iCheck -->
        <script src="{{SITE_URL.'plugins/iCheck/icheck.min.js' }}"></script>
        <script>
$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    })
})
        </script>
        <script>
            jQuery(document).ready(function () {
                jQuery('#myLoginform').validate({
                    rules: {
                        email: {
                            required: true,
                        }
                    },
                    messages: {
                        email: {
                            required: "Email should not be blank.",
                            email: "Please enter valid email id."
                        }

                    },
                });
            });
        </script>
    </body>
</html>
