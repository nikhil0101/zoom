@extends('layouts.master')
@section('title', 'sample')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">    
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Edit School Details</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Edit School Details</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="col-12">
            <div class="card card-info">
                <div class="card-header">
                    <span class="float-sm-left">Edit School Details</span>
                </div>
                <div class="card-body register-card-body">
                    <!-- <p class="login-box-msg">Please add event details</p> -->
                    {{ Form::open(array('url' => 'updateschoolinfo','id' => 'schoolform', 'name' => 'schoolinfo', 'method' => 'POST', 'files'=>true)) }}
                    @if ($errors->has('update'))
                    <div class="alert alert-danger">
                        {{ $errors->first('update') }}
                    </div>
                    @endif

                    <input type="hidden" class="form-control" value="{{ $schooldetail->id }}" name="id" placeholder="School name">

                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">School Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="{{ $schooldetail->school_name }}" name="school_name" placeholder="School name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">School Contact</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="{{ $schooldetail->school_contact }}" name="school_contact"  placeholder="School Contact">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">School Number</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="{{ $schooldetail->phone_number }}" name="phone_number"  placeholder="School Number">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">School Address</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="{{ $schooldetail->school_address }}" name="school_address"  placeholder="School Address">
                        </div>
                    </div>
                    <div class="form-group row right-marg-0 file-label-detail">
                        <label for="staticEmail" class="col-sm-2 col-form-label">School Logo</label>
                        <div class="custom-file col-sm-10">
                            <input type="hidden" name="oldImage" value="{{ $schooldetail->school_logo }}">
                            {!! Form::file('image', array('class' => 'custom-file-input', 'id' => 'exampleInputFile' )) !!}
                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label"></label>
                        <div class="custom-file col-sm-10">
                            @if(!empty($schooldetail->school_logo))
                            <img src="{{ asset('/images/'.$schooldetail->school_logo)}}" height='50px' width='50px'>
                            @else

                            @endif
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-info">Update</button>
                     <a href="{{SITE_URL}}schoollist" class="btn btn-default" role="button">Cancel</a>
                </div>
                {{ Form::close() }}
                
            </div>
        </div>
    </div>
</section>
@stop

@section('pagespecificscripts')
<!-- flot charts scripts-->
<script src="{{asset('js/schooldetails.js')}}"></script>
@stop