<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{SITE_URL.'dashboard' }}" class="brand-link">
<!--        <img src="../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">-->
        <img src="{{SITE_URL.'dist/img/AdminLTELogo.png' }}" alt="Zoom" class="brand-image img-circle elevation-3" style="opacity: .8">

        <span class="brand-text font-weight-light">Zoom</span>
    </a>       
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview ">
                    @if((Request::is('dashboard')))
                        @php
                            $active = 'active'
                        @endphp
                    @else
                        @php
                            $active = ''
                        @endphp
                    @endif
                    <a href="{{SITE_URL}}dashboard" class="nav-link {{ $active }}">
                        <i class="nav-icon fa fa-dashboard"></i>
                        <p>
                            Dashboard
                            <i class="right"></i>
                        </p>
                    </a>

                </li>

               @if (Auth::user()->user_roll == 1 || Auth::user()->user_roll == 2)
               <li class="nav-item has-treeview">
                   @if((Request::is('userlist')) || (Request::is('adduser')) || (Request::segment(1) == 'edituser'))
                        @php
                            $active = 'active'
                        @endphp
                    @else
                        @php
                            $active = ''
                        @endphp
                    @endif
                    <a href="{{SITE_URL}}userlist" class="nav-link {{ $active }}">
                        <i class="nav-icon fa fa-users"></i>
                        <p>User List</p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    @if((Request::is('schoollist')) || (Request::is('schoolcreate')) || (Request::segment(1) == 'editschooldetail'))
                        @php
                            $active = 'active'
                        @endphp
                    @else
                        @php
                            $active = ''
                        @endphp
                    @endif
                    <a href="{{SITE_URL}}schoollist" class="nav-link {{ $active }}">
                        <i class="nav-icon fa fa-university"></i>
                        <p>School Details</p>
                    </a>
                </li>
                @endif

                @if (Auth::user()->user_roll != 4)
                <li class="nav-item has-treeview">
                    @if((Request::is('events')) || ((Request::segment(1) == 'events') && (Request::segment(2) == 'create')) )
                        @php
                            $active = 'active'
                        @endphp
                    @else
                        @php
                            $active = ''
                        @endphp
                    @endif
                    <a href="{{SITE_URL}}events" class="nav-link {{ $active }}">
                        <i class="fa fa-calendar nav-icon"></i>
                        <p>Event List</p>
                    </a>
                </li>
                @endif
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
</aside>