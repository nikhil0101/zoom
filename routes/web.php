<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
/*
  |--------------------------------------------------------------------------
  | route to show the Dashboard
  |--------------------------------------------------------------------------
 */
Route::get('dashboard', array('uses' => 'LoginController@showDashboard'))->middleware('admin');
/*
  |--------------------------------------------------------------------------
  | 404 Page not found
  |--------------------------------------------------------------------------
 */
Route::get('pagenotfound', array('uses' => 'LoginController@ShowPagenotFound'));
/*
  |--------------------------------------------------------------------------
  | ShowLogin , Login , Logout
  |--------------------------------------------------------------------------
 */
// route to show the login form 
Route::get('/', array('uses' => 'LoginController@showLogin'));
// route to process the form
Route::post('login', array('uses' => 'LoginController@doLogin'));
// route to show the logout
Route::get('logout', array('uses' => 'LoginController@doLogout'));
/*
  |--------------------------------------------------------------------------
  | ShowRegister
  |--------------------------------------------------------------------------
 */
// route to show the register form 
Route::get('register', array('uses' => 'RegisterController@showRagister'));
// route to process the Registerform
Route::post('register', array('uses' => 'RegisterController@InsertRagisterData'));
/*
  |--------------------------------------------------------------------------
  | ResetPassword
  |--------------------------------------------------------------------------
 */
// route to show Forgot password form
Route::get('forgotpassword', array('uses' => 'ResetPasswordController@ShowForgotPassword'));
Route::post('forgotpassword', array('uses' => 'ResetPasswordController@CheckEmail'));
Route::get('resetpssword/{token}', array('uses' => 'ResetPasswordController@ResetPassword'));
Route::post('resetpssword', array('uses' => 'ResetPasswordController@UpdatePassword'));

/*
  |--------------------------------------------------------------------------
  | To the ShowUserList
  |--------------------------------------------------------------------------
 */
// route to show Userlist
Route::get('userlist', array('uses' => 'UserController@ShowUserList'))->middleware('admin');
Route::any('alluserlist', array('uses' => 'UserController@AllUserList'))->middleware('admin');
Route::get('edituser/{id}', array('uses' => 'UserController@PostShow'));
// route to Delete User
Route::post('deleteuser/{id}', array('uses' => 'UserController@DeteleUser'));
// route to Edit User
Route::name('edituser')->get('/edituser/{id}')->uses('UserController@EditUser');
// route to Update User
Route::post('updateuser', array('uses' => 'UserController@UpdateUser'));
// route to Add User
Route::get('adduser', array('uses' => 'UserController@ShowAddUser'))->middleware('admin');
// route to Add User
Route::post('adduser', array('uses' => 'UserController@AddUser'));
Route::post('changestatus', array('uses' => 'UserController@ChangeStatus'));
/*
  |--------------------------------------------------------------------------
  | To the ShowEvent
  |--------------------------------------------------------------------------
 */
// event resource route
Route::post('events/sportsList', array('uses' => 'EventController@sportsList'));
Route::post('alleventlist', array('uses' => 'EventController@AllEventList'));
Route::resource('events', 'EventController')->middleware('admin');

/*
  |--------------------------------------------------------------------------
  | To the School Detail
  |--------------------------------------------------------------------------
 */

Route::get('schoollist', array('uses' => 'SchooldetailController@index'))->middleware('admin');
Route::post('school_detail_list', array('uses' => 'SchooldetailController@ajaxlist'))->middleware('admin');
Route::get('schoolcreate', array('uses' => 'SchooldetailController@create'))->middleware('admin');
Route::post('addschoolinfo', array('uses' => 'SchooldetailController@store'));
Route::name('editschooldetail')->get('/editschooldetail/{id}')->uses('SchooldetailController@editschooldetails');
Route::post('updateschoolinfo', array('uses' => 'SchooldetailController@update'));
Route::get('deletesclinfo/{id}', array('uses' => 'SchooldetailController@destroy'));


/*
  |--------------------------------------------------------------------------
  | To the User Profile
  |--------------------------------------------------------------------------
 */
Route::get('showprofile', array('uses' => 'UserController@showprofile'));
