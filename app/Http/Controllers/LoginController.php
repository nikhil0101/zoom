<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;
use Hash;
use Redirect;
use DB;

class LoginController extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    function ShowPagenotFound() {
        return View('pages/login/404');
    }

    function showDashboard() {
        $Totalusers = DB::table('tbl_user')
                ->where('status', '1')
                ->count();

        $Totalevent = DB::table('tbl_event')
                ->where('status', '1')
                ->count();

        return View('layouts/default')->with('Totalusers', $Totalusers)->with('Totalevent', $Totalevent);
    }

    function showRagister() {
        return View('pages/login/register');
    }

    function showLogin() {
        if (Auth::check()) {
            return Redirect('dashboard');
        } else {
            return View('pages/login/login');
        }
    }

    function doLogin() {
        $rules = array(
            'email' => 'required|email',
            'password' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::to('/')->withErrors($validator)->withInput(Input::except('password'));
        } else {
            $input = Input::all();
            $email = $input['email'];
            
            $Loginuser = DB::table('tbl_user')
                    ->where('email', $email)
                    ->where('status', '==', '1')
                    ->count();
                //print_r($Loginuser);die;
            if ($Loginuser > 0) {
                return Redirect('')->withErrors(['invaliuser' => 'User invalid',]);
            } else {
                $userdata = array('email' => Input::get('email'), 'password' => Input::get('password'));
                if (Auth::attempt($userdata)) {
                    return Redirect('/dashboard');
                } else {
                    return Redirect('')->withErrors(['invalidpassword' => 'Invalid Username and Password',]);
                }
            }
        }
    }

    public function doLogout() {
        Auth::logout();
        return Redirect('/');
    }

}
