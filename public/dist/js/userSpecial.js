jQuery(document).ready(function () {
    var url = baseUrl + 'alluserlist';

    jQuery('#data').DataTable({
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "lengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
        "ajax": {
            "url": url,
            "dataType": "json",
            "type": "POST",
            "data": {'_token': csrf_token},
            "autoWidth": "true"
        },
        "columns": [
            // {"data": "id"},
            {"data": "full_name"},
            {"data": "email"},
            {"data": "phone"},
            {"data": "created_at"},
            {"data": "user_roll"},
            {"data": "status"},
            {"data": "action"}
        ],
        "columnDefs": [
            {"orderable": false, "targets": -1}
        ]

    });
    setTimeout(function () {
        jQuery('.alert').fadeOut(500)
    }, 2000);


    /* user module start*/
    $('#myAddform').validate({
        rules: {
            fullname: {
                required: true,
            },
            email: {
                required: true,
            },
            phone: {
                required: true,
                number: true,
                minlength: 9,
                maxlength: 10
            },
            user_roll: {
                required: true,
            }
        },
        messages: {
        },
        errorPlacement: function () {
            return false;  // suppresses error message text
        }
    });

    $('#myEditform').validate({
        rules: {
            fullname: {
                required: true,
            },
            email: {
                required: true,
            },
            phone: {
                required: true,
                number: true,
                minlength: 9,
                maxlength: 10
            },
            password: {
                required: true,
            }
        },
        messages: {
        },
        errorPlacement: function () {
            return false;  // suppresses error message text
        }
    });
    /* user module end*/
    
     $('#myProfileform').validate({
        rules: {
            fullname: {
                required: true,
            },
            email: {
                required: true,
            },
            phone: {
                required: true,
                number: true,
                minlength: 9,
                maxlength: 10
            },
            address: {
                required: true,
            }
        },
        messages: {
        },
        errorPlacement: function () {
            return false;  // suppresses error message text
        }
    });
    /* profile module end*/
});




function getConfirmation($id) {
    var delete_url = baseUrl + 'deleteuser/';
    var retVal = confirm("Are you sure?");
    if (retVal == true) {
        var url_id = $id;
        $.ajax({
            type: "POST",
            url: delete_url + url_id,
            data: {_token: csrf_token},
            success: function (data) {
                $('#divError').html('Users deleted successfully. <button type="button" onclick="$(this).parent().hide();" class="close" >×</button>');
                $('#divError').removeClass('alert-danger');
                $('#divError').addClass('alert-success');
                $('#divError').show(100);
                $('#divError').fadeOut(1000);
                $('#divError .close').show(100);
                $("html, body").animate({scrollTop: 0}, "fast");
                var table = $('#data').DataTable();
                table.ajax.reload();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }
    else
    {
        //odcument.write("User does not want to continue!");
        return false;
    }
}
function ChangeStatus(id) {
    var change_status_url = baseUrl + 'changestatus';
    $.ajax({
        type: "POST",
        url: change_status_url,
        data: {_token: csrf_token, id: id},
        success: function (data) {
            var table = jQuery('#data').DataTable();
            table.ajax.reload();
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
}